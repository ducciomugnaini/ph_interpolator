
%% -------------------------------------------------------------- preamble | Branch :: master
 
clear; clc; format compact; clf; close all;
 
%% ------------------------------------------------------- input parameters
 
idPHFig = 1;
idVFig  = 2; % Feedrate
idKFig  = 3; % Curvature
idAFig  = 4; % Acceleration
idJFig  = 5; % Jerk (Acceleration of Acceleration)
 
% ph Initial and Final points
pI = [1 1];
pF = [5 2];
 
% ph initial and final tangents
T0 = [1 1]*10;
T1 = [1 1]*20;

% ph solution
useSol_1    = false;
 
% Feedrate values (mm/sec)
Vm = 3.3;                 % commanded feedrate (m-antain)

VAi = 0;   VAf = Vm;    % Acceleration feedrate init/final
dVAi = 0;  dVAf = 0;    % first derivatives Acceleration feedrate init/final
ddVAi = 0; ddVAf = 0;   % second derivatives Acceleration feedrate init/final

VDi = Vm;   VDf = 0;    % Deceleration feedrate init/final
dVDi = Vm;  dVDf = 0;   % first derivatives Deceleration feedrate init/final
ddVDi = Vm; ddVDf = 0;  % second derivatives Deceleration feedrate init/final

% sampling time (delta t)
deltat = 0.01; % sec
Ts = deltat;
 
% Feedrate profile type
% 0 - C0
% 1 - C1
% 2 - C2
VType = 2;

% Tresholds 
Amax  = 800;     % mm/sec^2 Acceleration
Jmax  = 26000;   % mm/sec^2 Jerk
delta = 1;       % mu m (micrometro) chord-error
Ktrhld = 3;

useSigned_K = false;

% colors
PHColor   = 'k';
VaccColor = 'b';
VdecColor = 'g';
 
nTab = 100;
 
%% -------------------------------------------------------------- set input
 
% coefficients of feedrate profile
cVacc = []; % acceleration
cVdec = []; % deceleration
cVcon = []; % -
switch(VType)
    case 0
        cVacc = [cVacc VAi VAf];
        cVdec = [cVdec VDi VDf];
    case 1
        cVacc = [cVacc VAi VAi VAf VAf];
        cVdec = [cVdec VDi VDi VDf VDf];
    case 2
        cVacc = [cVacc VAi VAi VAi VAf VAf VAf];
        cVdec = [cVdec VDi VDi VDi VDf VDf VDf];
end
 
%% ---------------------------------------------------------------- ph init
 
myPH = phCurve(pI, pF, T0, T1, useSol_1);
 
[xi, Cxi] = myPH.computeCurve(nTab);
 
DL  = myPH.computeDisplacementLength();
AL  = myPH.computeArcLength();
CPL = myPH.computeContrPolyLength();
 
%% ----------------------------------------------------- compute Na, Nd, Nc

[par, K, xiKPeaks, KPeaks] = myPH.computeCurvature(useSigned_K,100);
myPH.plotCurvature(par, K, 'k', xiKPeaks, KPeaks, 'r', idKFig);

% ---- Step I: detection of critical points
K_thr1 = 8*delta /((Vm*Ts)^2 + 4*delta^2);
K_thr2 = Amax/Vm^2;
K_thr3 = sqrt(Jmax/Vm^3);
Ktrhld = min([K_thr1, K_thr2, K_thr3]);
figure(idKFig); hold on;
plot([0,1],[Ktrhld Ktrhld],':k');
hold off;


%% ----

% ---- define Sacc, Sdec (<- to custom)
Sacc = AL/4;
Sdec = AL/4;
Scon = AL - (Sacc + Sdec);

% ---- compute F coefficients for ACC/DEC-eleration phase 
cFacc = phCurve.computeFcoeff(cVacc);
disp(['coefficients of Facc: ' num2str(cFacc)]);

cFdec = phCurve.computeFcoeff(cVdec);
disp(['coefficients of Fdec: ' num2str(cFdec)]);
 
% ---- compute Times (Time acceleration, deceleration, constant) (pag. 635)
 
% Tacc = 2*Sacc/Vm;
% Tdec = 2*Sdec/Vm;
Tacc = Sacc/cFacc(end);
Tdec = Sdec/cFdec(end);
Tcon = (AL/Vm) - ((Tacc+Tdec)/2);
T = Tacc + Tdec + Tcon;
disp('>> Acc | Con | Dec Times | Total Time')
disp([num2str(Tacc) ' -> ' num2str(Tcon) ' -> ' num2str(Tdec) ' = ' num2str(T)]); 
 
% ---- display feedrate profile
 
ta = linspace(0,Tacc,100); taN = linspace(0,1,100);
td = linspace(0,Tdec,100); tdN = linspace(0,1,100);
tc = linspace(0,Tcon,100); tcN = linspace(0,1,100);

% -- acceleration profile (V, V', V'')
[Vta] = phCurve.evalFeedRateProfile(cVacc, taN);
[DVta] = phCurve.evalDerFeedrate(cVacc, taN);
if VType ~= 0
    [DDVta] = phCurve.evalDerDerFeedrate(cVacc, taN);
end

phCurve.plotFeedRateProfile(ta, Vta,VaccColor,idVFig);
phCurve.plotDerFeedrate(ta, DVta,VaccColor,idAFig);
if VType ~= 0
    phCurve.plotDerFeedrate(ta, DDVta,VaccColor,idJFig);
end

% -- deceleration profile (V, V', V'')
[Vtd] = phCurve.evalFeedRateProfile(cVdec, tdN);
[DVtd] = phCurve.evalDerFeedrate(cVdec, tdN);
if VType ~= 0
    [DDVtd] = phCurve.evalDerDerFeedrate(cVdec, tdN);
end

phCurve.plotFeedRateProfile(Tacc+Tcon+td, Vtd,VdecColor,idVFig);
phCurve.plotDerFeedrate(Tacc+Tcon+td, DVtd,VdecColor, idAFig);
if VType ~= 0
    phCurve.plotDerFeedrate(Tacc+Tcon+td, DDVtd,VdecColor,idJFig);
end

% -- compute reference parameter values for ACCeleration phase
 
% number of sampling time for acceleration
XiK(1) = [0];
N_Xi = floor(Tacc/deltat);
incr = deltat/Tacc;
for k = 1:N_Xi+1
    
    % normalized sampling time to evaluate integral F of (29.21)
    tkN = (k-1)*incr;
    
    F_tkN = Tacc * phCurve.DeCasteljau(cFacc,tkN);
    
    XiK(k+1) = myPH.newtonXi(F_tkN, XiK(k), 100, exp(-5));
    tkN;
end
XiK;
 
% number of sampling time for decelaration
XiKdec(1) = XiK(end);
N_Xi = floor(Tdec/deltat);
for k = 1:N_Xi+1
    
    % normalized sampling time to evaluate integral F of (29.21)
    tkN = (k-1)*deltat/Tdec;
    % tkN = ((k-1)*deltat - (Tacc+Tcon))/()
    
    F_tkN = Tdec * phCurve.DeCasteljau(cFdec,tkN) + (Sacc + Scon);%(AL/3*2);
    
    XiKdec(k+1) = myPH.newtonXi(F_tkN, XiKdec(k), 100, exp(-5));
    
end
XiKdec(1) = XiKdec(2); %update the effective starting value
 
XiKdec;

% -- Lengths
AccLength = phCurve.DeCasteljau(myPH.s, XiK(end)) - phCurve.DeCasteljau(myPH.s, XiK(1));
ConLength = phCurve.DeCasteljau(myPH.s, XiKdec(2)) - phCurve.DeCasteljau(myPH.s, XiK(end));
DecLength = phCurve.DeCasteljau(myPH.s, 1) - phCurve.DeCasteljau(myPH.s, XiKdec(2));
fprintf ( 1, 'ARC Length:\n - Sacc %3.5f | %3.5f \n - Scon %3.5f | %3.5f\n - Sdec %3.5f | %3.5f\n - Stot %3.5f | %3.5f\n', ...
    Sacc, AccLength, Scon, ConLength, Sdec, DecLength, AL, AccLength + DecLength + ConLength); 
% AccLength + DecLength + ConLength
% AL
 
%% ------------------------------------------------------------ plotting ph
disp('>> show results');
 
% ---- ph 
% myPH.plotDispacement(1,'b');
% myPH.plotControlPolygon(1,'b');
phCurve.plotCurve(Cxi, PHColor, 3, idPHFig);

% ---- ph curvature peaks on ph curve
for i=1:size(xiKPeaks,2)
    [xi, Cxi] = myPH.computePointOnCurve(xiKPeaks(i));
    figure(idPHFig); hold on;
    plot(Cxi(1),Cxi(2), 'or');
    hold off;
end

% ---- interpolation points 
myPH.plotInterpReferencePoints(XiK,'y',idPHFig);
myPH.plotInterpReferencePoints(XiKdec,'g',idPHFig);
 
% -- graphic settings for PH CURVE figure
figure(idPHFig); hold on;
axis equal; 
grid on; grid minor;
set(gcf, 'units','normalized','outerposition',[0 0.55 0.5 0.4]);
set(gcf, 'Color', [1 1 1]);
title('ph curve');
hold off;

% -- graphic settings for ph CURVATURE figure
figure(idKFig); hold on;
grid on; grid minor;
set(gcf, 'units','normalized','outerposition',[0.55 0.55 0.45 0.4]);
set(gcf, 'Color', [1 1 1]);
title('ph curve curvature');
hold off;
 
% -- graphic settings for FEEDRATE (V) profile figure
Vfig = figure(idVFig); hold on;
% axis equal; 
grid on; grid minor;
set(gcf, 'units','normalized','outerposition',[0 0 0.33 0.5]);
set(gcf, 'Color', [1 1 1]);
title('feedrate (V)');
hold off;
 
% -- graphic settings for ACCELERATION (V') profile figure
figure(idAFig); hold on;
% axis equal; 
grid on; grid minor;
set(gcf, 'units','normalized','outerposition',[0.33 0 0.33 0.5]);
set(gcf, 'Color', [1 1 1])
title('acceleration (dV)');
hold off;

% -- graphic settings for JERK (V'') profile figure
figure(idJFig); hold on;
% axis equal; 
grid on; grid minor;
set(gcf, 'units','normalized','outerposition',[0.66 0 0.33 0.5]);
set(gcf, 'Color', [1 1 1]);
title('jerk (ddV)');
hold off;

% movegui(idPHFig,'northwest');
% movegui(idKFig, 'southwest');
% movegui(idVFig, 'northeast');
% movegui(idAFig, 'east');
% movegui(idJFig, 'southeast');
 
 
 
%% ----------------------------------------------------------------------- | Branch :: master
 

