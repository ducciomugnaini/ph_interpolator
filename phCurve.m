classdef phCurve
    
    properties
        
        U       = [];
        V       = [];
        W       = [];
        sigma   = [];
        s       = [];
        L       = [];
        CPcmplx = [];
        CP      = [];   % 2 x #ControlPoint
        
        Kdata = [];
        
    end
    
    methods
        
        function this = phCurve(pI, pF, T0, T1, useSol_1)
        
            % ---- compute Ph Quintic by hermite interpolation
            
            d0 = complex(T0(1), T0(2));
            d1 = complex(T1(1), T1(2));
            
            pI = complex(pI(1), pI(2));
            pF = complex(pF(1), pF(2));
            
            % -- pag. 530 (25.18)
            w0 = sqrt(d0);
            w2 = sqrt(d1);
            
            coeff_1 = 2/3;
            coeff_2 = w0 + w2;
            coeff_3 = w0^2 + w2^2 + (w2*w0)/3 - 5*(pF-pI);
            
            w1 = roots([coeff_1 coeff_2 coeff_3]);
            
            if(useSol_1)
                w1 = w1(1);
            else
                w1 = w1(2);
            end
            
            % -- pag. 415 (19.12)
            p1 = pI + 1/5 * w0^2;
            p2 = p1 + 1/5 * w0*w1;
            p3 = p2 + 1/15 * (2*w1^2 + w0*w2);
            p4 = p3 + 1/5 * w1*w2;
            p5 = p4 + 1/5 * w2^2;
            CP = [pI p1 p2 p3 p4 p5];
            this.CPcmplx = CP;
            
            this.CP = [real(this.CPcmplx); 
                       imag(this.CPcmplx)];
            
            % -- save w,u,v coefficents
            W = [w0 w1 w2];
            U = [real(w0) real(w1) real(w2)];
            V = [imag(w0) imag(w1) imag(w2)];
            this.W = W;
            this.U = U;
            this.V = V;
            
            % -- save sigmas            
            this.sigma(1) = U(1)^2 + V(1)^2;
            this.sigma(2) = U(1)*U(2) + V(1)*V(2);
            this.sigma(3) = 2/3* (U(2)^2 + V(2)^2) + 1/3*(U(1)*U(3) + V(1)*V(3));
            this.sigma(4) = U(2)*U(3) + V(2)*V(3);
            this.sigma(5) = U(3)^2 + V(3)^2;   
            
            % -- save arc length coefficients s
            this.s(1) = 0;
            this.s(2) = 1/5 * (this.sigma(1));
            this.s(3) = 1/5 * (this.sigma(1) + this.sigma(2));
            this.s(4) = 1/5 * (this.sigma(1) + this.sigma(2) + this.sigma(3));
            this.s(5) = 1/5 * (this.sigma(1) + this.sigma(2) + this.sigma(3) + this.sigma(4));
            this.s(6) = 1/5 * (this.sigma(1) + this.sigma(2) + this.sigma(3) + this.sigma(4) + this.sigma(5));
            
        end
        
        % -- computation function
        
        function xiNext = newtonXi(this, F_kt, xiCurr, itMax, tolx)
            
            fx  = phCurve.DeCasteljau(this.s,     xiCurr) - F_kt;
            f1x = phCurve.DeCasteljau(this.sigma, xiCurr);            
            xiNext = xiCurr - (fx / f1x);
            
            i = 0;
            
            while(i < itMax && abs(xiNext - xiCurr) > tolx)
                
                i = i+1;
                xiCurr = xiNext;
                
                fx  = phCurve.DeCasteljau(this.s,     xiCurr) - F_kt;
                f1x = phCurve.DeCasteljau(this.sigma, xiCurr);
                xiNext = xiCurr - (fx / f1x);
                
            end
            
            if abs(xiNext - xiCurr) > tolx
               error(['>> no convergence -> xiCurr: ' num2str(xiCurr) ' | xiNext: ' num2str(xiNext) '\n']);
            end
            
        end
        
        function [xi, Cxi] = computeCurve(this, nTab)
            
            xi = linspace(0,1, nTab);
            Cxi = [];
            
            for i = 1:nTab
                Cxi(:,i) = phCurve.DeCasteljau(this.CP,xi(i));
            end
            
        end
        
        function [xi, Cxi] = computePointOnCurve(this, xi)
            Cxi = phCurve.DeCasteljau(this.CP,xi);
        end
        
        function CPL = computeContrPolyLength(this)           
            CPL = 0;
            for i=1:size(this.CP,2)-1
                CPL = CPL + norm(this.CP(:,i+1) - this.CP(:,i));                
            end            
        end
        
        function DL = computeDisplacementLength(this)           
            DL = norm(this.CP(:,end) - this.CP(:,1));            
        end
        
        function AL = computeArcLength(this)
            AL = sum(this.sigma)/(size(this.CP,2)-1);  
        end
                
        function [xi, K, xiMax, Kmax] = computeCurvature(this, isSigned,nTab)
            
            xi = linspace(0,1,nTab);
            xiMax = [];
            K = [];
            Kmax = [];
            
            % -- implementation of pag.388 (17.16)
            deltaU = 2*[this.U(2)-this.U(1) this.U(3)-this.U(2)];
            deltaV = 2*[this.V(2)-this.V(1) this.V(3)-this.V(2)];
            for i = 1:nTab
                
                Ut = phCurve.DeCasteljau(this.U, xi(i));
                Vt = phCurve.DeCasteljau(this.V, xi(i));
                U1t = phCurve.DeCasteljau(deltaU, xi(i));
                V1t = phCurve.DeCasteljau(deltaV, xi(i));
                sigma2 = phCurve.DeCasteljau(this.sigma, xi(i))^2;
                
                K(i) = 2 * (Ut*V1t - U1t*Vt)/(sigma2);
                
                if(~isSigned)
                    K(i) = abs(K(i));
                    % -- curvature peaks detection
                    if( i>3 && K(i-2)<K(i-1) && K(i-1)>K(i))
                        xiMax = [xiMax xi(i-1)];
                        Kmax = [Kmax K(i-1)];
                    end
                end
            end
            
        end
        
        function [xi, T] = computeParametricSpeed(this, nTab)
            
        end
    
        % -- graphic function
        
        function plotControlPolygon(this, idFig, colorCode)
            figure(idFig); hold on;
            plot(this.CP(1,:), this.CP(2,:),['-o' colorCode]);
            hold off;
        end
        
        function plotDispacement(this, idFig, colorCod)
            figure(idFig); hold on;
            plot([this.CP(1,1) this.CP(1,end)], [this.CP(2,1) this.CP(2,end)], ['-o' colorCod]);
            hold off;
            
        end
        
        function plotInterpReferencePoints(this, Xi, colCod, idFig)
            
            figure(idFig); hold on;
            for i=1:size(Xi,2)
                
                Cxi = phCurve.DeCasteljau(this.CP, Xi(i));
                plot(Cxi(1), Cxi(2),['.' colCod]);
                
            end
            hold off;
            
        end
        
        function plotCurvature(this, xi, K, KColCode, xiMax, KMax, peaksColCode, idFig)
            
            figure(idFig); hold on;
            plot(xi, K, ['-' KColCode]);
            plot(xiMax, KMax, ['o' peaksColCode]);
            hold off;
            
        end
        
    end
    
    methods (Static)
       
        % 
        function Cu = DeCasteljau(P,u)
           
            n = size(P,2);
            Q = [];
            for i= 1:n
                Q(:,i) = P(:,i);
            end
            
            for k = 1:n
               for i = 1: n-k
                   Q(:,i) = (1-u)*Q(:,i) + u*Q(:,i+1);
               end
            end
            
            Cu = Q(:,1);
        end
        
        function plotCurve(Cxi, colorCode, thick,idFig)
            figure(idFig); hold on;
            plot(Cxi(1,:), Cxi(2,:),['-' colorCode], 'LineWidth', thick);
            hold off;
        end
        
        function [evalVti] = evalFeedRateProfile(V,ti)
           
            evalVti = zeros(1,size(ti,2));
            for i=1:size(ti,2)
                Vti = phCurve.DeCasteljau(V,ti(i));
                evalVti(i) = Vti;
            end
            
        end
        
        function [evalV1ti] = evalDerFeedrate(V,ti)
           
            n = size(V,2)-1;
            deltaV = [];
            for i = 1:n
                deltaV(i) = V(i+1) - V(i);
            end
            deltaV = n*deltaV;
            
            evalV1ti = [];
            for i = 1:size(ti,2)
                evalV1ti(i) = phCurve.DeCasteljau(deltaV, ti(i));            
            end
            
        end
        
        function [evalV2ti] = evalDerDerFeedrate(V,ti)
           
            n = size(V,2)-2;
            deldelV = [];
            for i = 1:n
                deldelV(i) = V(i+2) - 2*V(i+1) + V(i);
            end
            deldelV = n*(n+1)*deldelV;
            
            evalV2ti = [];
            for i = 1:size(ti,2)
                evalV2ti(i) = phCurve.DeCasteljau(deldelV, ti(i));            
            end
            
        end
        
        function plotFeedRateProfile(ti,Vti, colCode, idFig)
            figure(idFig); hold on;
            plot(ti, Vti, ['-' colCode]);
            hold off;
        end
        
        function plotDerFeedrate(ti, DVti, colCode, idFig)
            figure(idFig); hold on;
            plot(ti, DVti, ['-' colCode]);
            hold off;
        end
        
        function [coeffF] = computeFcoeff(coeffV)
            coeffF = [];
            coeffF(1) = 0;
            FDeg = size(coeffV,2);
            FNumCoeff = size(coeffV,2)+1;
            for i = 2:FNumCoeff
                coeffF(i) = 0;
                for j = 1:(i-1)
                    coeffF(i) = coeffF(i) + coeffV(j);
                end
                coeffF(i) = coeffF(i)/FDeg;
            end
        end
        
    end
    
    
        
        
    
end

